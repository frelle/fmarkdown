module FMarkdown

open System.Text
open System.Text.RegularExpressions

type Element =
    | H1
    | H2
    | H3
    | H4
    | H5
    | H6
    | Paragraph
    | Bold
    | Italicized
    | InlineCode
    | CodeBlock of programmingLanguage: string option

module Element =
    let attributes element =
        match element with
        | H1
        | H2
        | H3
        | H4
        | H5
        | H6
        | Paragraph
        | Bold
        | Italicized
        | InlineCode -> []
        | CodeBlock programmingLanguage ->
            match programmingLanguage with
            | None -> []
            | Some pl -> [ "class", [ $"lang-{pl}" ] ]

    let tagName element =
        match element with
        | H1 -> "h1"
        | H2 -> "h2"
        | H3 -> "h3"
        | H4 -> "h4"
        | H5 -> "h5"
        | H6 -> "h6"
        | Paragraph -> "p"
        | Bold -> "b"
        | Italicized -> "i"
        | InlineCode -> "code"
        | CodeBlock _ -> "pre"

    let openingTag element =
        let tagName = tagName element

        match attributes element with
        | [] -> $"<{tagName}>"
        | attributes ->
            let attributesString =
                attributes
                |> List.map
                    (fun (attributeName, attributeValues) ->
                        let value = attributeValues |> String.concat " "
                        $"{attributeName}=\"{value}\"")
                |> String.concat " "

            $"<{tagName} {attributesString}>"

    let closingTag element =
        let tagName = tagName element
        $"</{tagName}>"


let skipSpaces (s: string byref) =
    if s <> "" then
        let mutable head = s.[0]

        while head = ' ' && s <> "" do
            s <- s.[1..]
            head <- s.[0]

module String =
    let takeWhile f (s: string) =
        let rec takeWhile' (acc: StringBuilder) (remaining: string) =
            match remaining |> Seq.tryHead with
            | None -> acc.ToString ()
            | Some c when not (f c) -> acc.ToString ()
            | Some c -> takeWhile' (acc.Append c) remaining.[1..]

        takeWhile' (StringBuilder ()) s

    let takeUntil (sequence: string) (s: string) =
        let sequenceAsCharSequence = sequence |> Array.ofSeq

        let indexOfFirstAppearanceOfSequence =
            s
            |> Seq.windowed (sequence |> String.length)
            |> Seq.tryFindIndex (fun s -> s = sequenceAsCharSequence)

        match indexOfFirstAppearanceOfSequence with
        | None -> s
        | Some i -> s.[..(i - 1)]


let public parse (input: string) =
    // We'll need a list and a string builder
    let mutable output = StringBuilder ()
    let mutable stack = []

    let mutable s = input

    while s <> "" do
        let currentElement = stack |> List.tryHead

        let isInCode =
            match currentElement with
            | Some InlineCode
            | Some (CodeBlock _) -> true
            | _ -> false

        if s.StartsWith "###### " then
            if currentElement = None then
                stack <- H6 :: stack
                output <- output.Append (H6 |> Element.openingTag)
            else
                output <- output.Append "###### "

            s <- s.[7..]
            skipSpaces &s

        elif s.StartsWith "##### " then
            if currentElement = None then
                stack <- H5 :: stack
                output <- output.Append (H5 |> Element.openingTag)
            else
                output <- output.Append "##### "

            s <- s.[6..]
            skipSpaces &s

        elif s.StartsWith "#### " then
            if currentElement = None then
                stack <- H4 :: stack
                output <- output.Append (H4 |> Element.openingTag)
            else
                output <- output.Append "#### "

            s <- s.[5..]
            skipSpaces &s

        elif s.StartsWith "### " then
            if currentElement = None then
                stack <- H3 :: stack
                output <- output.Append (H3 |> Element.openingTag)
            else
                output <- output.Append "### "

            s <- s.[4..]
            skipSpaces &s

        elif s.StartsWith "## " then
            if currentElement = None then
                stack <- H2 :: stack
                output <- output.Append (H2 |> Element.openingTag)
            else
                output <- output.Append "## "

            s <- s.[3..]
            skipSpaces &s

        elif s.StartsWith "# " then
            if currentElement = None then
                stack <- H1 :: stack
                output <- output.Append (H1 |> Element.openingTag)
            else
                output <- output.Append "# "

            s <- s.[2..]
            skipSpaces &s

        elif s.StartsWith "\n" then
            match currentElement with
            | None -> ()
            | Some (CodeBlock _) -> output <- output.Append '\n'
            | Some e ->
                stack <- stack |> List.tail
                output <- output.Append (e |> Element.closingTag)

            s <- s.[1..]
            skipSpaces &s

        elif s.StartsWith "**" then
            if isInCode then
                output <- output.Append "**"
            else
                match currentElement with
                | Some Bold ->
                    stack <- stack |> List.tail
                    output <- output.Append (Bold |> Element.closingTag)
                | Some _
                | None ->
                    // Add bold to stack
                    stack <- Bold :: stack
                    output <- output.Append (Bold |> Element.openingTag)

            s <- s.[2..]

        elif s.StartsWith "*" then
            if isInCode then
                output <- output.Append "*"
            else
                match currentElement with
                | Some Italicized ->
                    stack <- stack |> List.tail
                    output <- output.Append (Italicized |> Element.closingTag)
                | Some _
                | None ->
                    stack <- Italicized :: stack
                    output <- output.Append (Italicized |> Element.openingTag)

            s <- s.[1..]

        elif s.StartsWith "```" then
            s <- s.[3..]

            // if already in code block, close it
            match currentElement with
            | Some _ -> output <- output.Append "```"
            | None ->
                // Parse the programming language by parsing everything
                // until and including the next linebreak.
                let programmingLanguage =
                    s |> String.takeWhile (fun c -> c <> '\n')

                let element =
                    if programmingLanguage.Length > 0 then
                        CodeBlock (Some programmingLanguage)
                    else
                        CodeBlock None

                output <- output.Append (element |> Element.openingTag)

                // Skip programming language string and linebreak
                s <- s.[programmingLanguage.Length..]

                let closingSequence = "\n```"
                let untilClosingSequence = s |> String.takeUntil closingSequence
                let blockContents = untilClosingSequence.Trim ()

                output <- output.Append blockContents
                output <- output.Append (element |> Element.closingTag)

                s <- s.[untilClosingSequence.Length..]
                s <- s.[closingSequence.Length..]

        elif s.StartsWith "`" then
            match currentElement with
            | Some InlineCode ->
                stack <- stack |> List.tail
                output <- output.Append (InlineCode |> Element.closingTag)
            | Some _
            | None ->
                stack <- InlineCode :: stack
                output <- output.Append (InlineCode |> Element.openingTag)

            s <- s.[1..]

        elif s.StartsWith "\\" then
            match s |> Seq.tryItem 1 with
            | Some escapedChar ->
                output <- output.Append escapedChar
                s <- s.[2..]
            | None -> s <- s.[1..]

        elif s.StartsWith "<" then
            match currentElement with
            | None ->
                // Try to parse HTML code
                // if begins with anything but letter, abort
                s <- s.[1..]
                let stringBeginsWithLetter = (s, @"^[A-Za-z]") |> Regex.Match |> (fun m -> m.Success)
                if not stringBeginsWithLetter then
                    output <- output.Append "<"
                else
                    let tagName = s |> String.takeWhile (fun c -> "\n\r >" |> Seq.contains c |> not)
                    s <- s.[tagName.Length..]
                    let closingTag = $"</{tagName}>"
                    let code = s |> String.takeUntil closingTag
                    s <- s.[code.Length..]
                    s <- s.[closingTag.Length..]

                    output <- output.Append($"<{tagName}{code}{closingTag}")
            | Some _ ->
                output <- output.Append "<"
                s <- s.[1..]

        else
            let firstChar = s.[0] |> string

            match currentElement with
            | None ->
                stack <- Paragraph :: stack
                output <- output.Append (Paragraph |> Element.openingTag)
                output <- output.Append firstChar
            | Some _ -> output <- output.Append firstChar

            s <- s.[1..]

    for e in stack do
        output <- output.Append (e |> Element.closingTag)

    output.ToString ()
